package fucoin.setup;

public class SelectableNetworkInterface {

    String hostName;
    String interfaceName;

    public SelectableNetworkInterface(String hostName, String interfaceName) {
        this.hostName = hostName;
        this.interfaceName = interfaceName;
    }

    public String getHostName() {
        return hostName;
    }

    public String getInterfaceName() {
        return interfaceName;
    }

    @Override
    public String toString() {
        return getInterfaceName() + " (" + getHostName() + ")";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SelectableNetworkInterface) {
            return ((SelectableNetworkInterface) obj).getHostName().equals(this.getHostName());
        }
        return super.equals(obj);
    }
}
