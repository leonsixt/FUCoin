package fucoin;

import akka.actor.ActorRef;

public class SnapshotToken {
    public String token;
    public ActorRef observer;

    public SnapshotToken(String token, ActorRef observer) {
        this.token = token;
        this.observer = observer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SnapshotToken)) return false;

        SnapshotToken that = (SnapshotToken) o;

        if (token != null ? !token.equals(that.token) : that.token != null) return false;
        return observer != null ? observer.equals(that.observer) : that.observer == null;
    }

    @Override
    public int hashCode() {
        int result = token != null ? token.hashCode() : 0;
        result = 31 * result + (observer != null ? observer.hashCode() : 0);
        return result;
    }

}
