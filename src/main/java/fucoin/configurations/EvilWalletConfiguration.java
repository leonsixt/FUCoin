package fucoin.configurations;

import akka.actor.ActorRef;
import akka.dispatch.OnSuccess;
import akka.pattern.Patterns;
import fucoin.actions.persist.ActionSearchMyWallet;
import fucoin.actions.persist.ActionSearchMyWalletAnswer;
import fucoin.actions.transaction.ActionGetAmount;
import fucoin.actions.transaction.ActionGetAmountAnswer;
import fucoin.configurations.internal.ConfigurationName;
import scala.concurrent.Await;
import scala.concurrent.Future;

import java.util.Collections;
import java.util.List;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.dispatch.Futures;
import akka.pattern.Patterns;
import akka.util.Timeout;
import fucoin.AbstractNode;
import fucoin.actions.control.ActionAddOverlayNeighbours;
import fucoin.actions.control.ActionAnnounceWalletCreation;
import fucoin.actions.control.ActionWalletSendMoney;
import fucoin.actions.join.ActionTellSupervisor;
import fucoin.actions.transaction.ActionGetAmount;
import fucoin.actions.transaction.ActionGetAmountAnswer;
import fucoin.actions.transaction.ActionNotifyObserver;
import fucoin.configurations.internal.ConfigurationCreator;
import fucoin.configurations.internal.NodeHelper;
import fucoin.supervisor.SuperVisorImpl;
import fucoin.wallet.WalletImpl;
import org.gephi.graph.api.Edge;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.Node;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.Promise;
import scala.concurrent.duration.Duration;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
/**
 * This configuration spawns 200 wallets to demonstrate the spawning of many headless wallets
 */
@ConfigurationName("EvilWallet")
public class EvilWalletConfiguration extends AbstractConfiguration {
    @Override
    public void run() {
        initSupervisor();
        try {
            spawnWallets(100, false);
            System.out.println("Wallet spawning done!");
        } catch (Exception e) {
            System.out.println("Wallet spawning timed out!");
        }

        List<ActorRef> wallets = wallets();

        ActorRef evil = wallets.get(0);
        for(int i = 1; i < wallets.size(); i++) {
            ActorRef sender = wallets.get(i);
            Future<Object> future = Patterns.ask(evil, new ActionGetAmount(), timeout);
            ActionGetAmountAnswer answer = null;
            try {
                answer = (ActionGetAmountAnswer) Await.result(future, timeout.duration());
            } catch (Exception e) {
                e.printStackTrace();
            }
            int transferAmount = answer.amount;
            System.out.println(evil.path().name());
            sender.tell(new ActionWalletSendMoney(evil.path().name(), transferAmount, self()), self());
        }
    }

    @Override
    public void onReceive(Object message) {
        super.onReceive(message);
        System.out.println(message);
    }
}
