package fucoin.configurations;

import akka.actor.ActorRef;
import akka.dispatch.Futures;
import akka.dispatch.OnComplete;
import akka.dispatch.OnSuccess;
import akka.pattern.Patterns;
import fucoin.Snapshot;
import fucoin.actions.control.ActionWalletSendMoney;
import fucoin.actions.snap.SnapAction;
import fucoin.actions.transaction.ActionGetAmount;
import fucoin.actions.transaction.ActionGetAmountAnswer;
import fucoin.actions.transaction.ActionNotifyObserver;
import fucoin.configurations.internal.ConfigurationName;
import org.apache.commons.math3.distribution.EnumeratedDistribution;
import org.apache.commons.math3.util.Pair;
import org.json.JSONObject;
import scala.Function1;
import scala.PartialFunction;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.Promise;
import scala.runtime.BoxedUnit;
import scala.util.Try;

import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.openide.util.Exceptions;

/**
 * This configuration spawns 200 wallets to demonstrate the spawning of many headless wallets
 */

class WalletSimulation {
    private double p_transaction;
    private double min_value;
    private double max_value;
    private double adaption_factor;
    private String name;

    private Map<String, Double> factorReceiver;

    public WalletSimulation(String name,
                            List<String> walletNames,
                            double p_transaction, double min_value, double max_value, double adaption_factor) {
        this.p_transaction = p_transaction;
        this.min_value = min_value;
        this.max_value = max_value;
        this.adaption_factor = adaption_factor;
        this.name = name;

        int n = walletNames.size();

        this.factorReceiver = new HashMap<String, Double>();
        for (String walletName : walletNames) {
            if (walletName.equals(this.name)) {
                continue;
            }
            this.factorReceiver.put(walletName, 1.);
        }
    }
    
    public boolean willSend(){
        return Math.random()<=p_transaction;
    }
    
    public String getName(){
        return name;
    }
    
    public int sampleValue(int currentAmount) {
        double random = Math.random() * (this.max_value - this.min_value) + this.min_value;
        int value =  (int) Math.round(currentAmount * random);
        assert value <= currentAmount;
        return value;
    }
    private double total_factor() {
        double sum = 0;
        for (double value : this.factorReceiver.values()) {
            sum += value;
        }
        return sum;
    }

    public String sampleReceiver() {
        double normalization = total_factor();
        List<Pair<String, Double>> distribution = new ArrayList<Pair<String, Double>>();
        for (String walletName : this.factorReceiver.keySet()) {
            double prob = this.factorReceiver.get(walletName) / normalization;
            distribution.add(new Pair(walletName, prob));
        }
        return new EnumeratedDistribution<>(distribution).sample();
    }

    public void updateProbabily(String walletName, int amountGotten) {
        double currentFactor = this.factorReceiver.get(walletName);
        this.factorReceiver.put(walletName, currentFactor + amountGotten * this.adaption_factor);
    }
}

@ConfigurationName("GGBWallet")
public class GGBWalletConfiguration extends AbstractConfiguration {
    private static int n_generous = 3;
    private static int n_greedy = 3;
    private static int n_balanced = 3;
    private static int n_steps = 500;
    private static String logDirectory = "log";

    private Map<String, WalletSimulation> simulations;
    private HashMap<String,ActorRef> walletMap;

    private static final double GENEROUS_PROBABILITY = 0.02;
    private static final double GENEROUS_MIN =  0.2;
    private static final double GENEROUS_MAX =  0.8;
    private static final double GENEROUS_ADAPTION = 0.1;


    private static final double GREEDY_PROBABILITY = 0.005;
    private static final double GREEDY_MIN =  0.05;
    private static final double GREEDY_MAX =  0.5;
    private static final double GREEDY_ADAPTION = 0.01;

    private static final double BALANCED_PROBABILITY = 0.005;
    private static final double BALANCED_MIN =  0.05;
    private static final double BALANCED_MAX =  0.5;
    private static final double BALANCED_ADAPTION = 0.01;

    private int currentStep = 0;
    private Promise<Void> simulationFinished = Futures.promise();

    static {
        File logDir = new File(logDirectory);
        if (!logDir.exists()) {
            logDir.mkdir();
        }
    }
    @Override
    public void run() {
        simulations = new HashMap<>();
        initSupervisor();

        List<String> generousWallets = new ArrayList<>();
        List<String> greedyWallets = new ArrayList<>();
        List<String> balancedWallets = new ArrayList<>();
        walletMap = new HashMap<>();
        
        try {
            for (int i = 0; i < n_generous; i++)  {
                String name = "generous_" + i;
                walletMap.put(name, spawnWallet(name, false));
                generousWallets.add(name);
            }
            for (int i = 0; i < n_greedy; i++)  {
                String name = "greedy_" + i;
                walletMap.put(name, spawnWallet(name, false));
                greedyWallets.add(name);
            }
            for (int i = 0; i < n_balanced; i++)  {
                String name = "balanced_" + i;
                walletMap.put(name, spawnWallet(name, false));
                balancedWallets.add(name);
            }
            System.out.println("Wallet spawning done!");
        } catch (Exception e) {
            System.out.println("Wallet spawning timed out!");
        }
        for (String name : walletMap.keySet()) {
            this.addKnownNeighbor(name, walletMap.get(name));
        }
        List<String> allWalletNames = Stream.concat(generousWallets.stream(),
                greedyWallets.stream()).collect(Collectors.toList());

        allWalletNames = Stream.concat(allWalletNames.stream(),
                balancedWallets.stream()).collect(Collectors.toList());
        for (String name : generousWallets) {
            simulations.put(name, new WalletSimulation(name, allWalletNames, GENEROUS_PROBABILITY,
                    GENEROUS_MIN, GENEROUS_MAX, GENEROUS_ADAPTION));
            // initialize WalletSimulation
            //          Money   Probability  Factor
            // greedy     20%      0.5%        1 %
            // generous   70%      2%         10 %
            // balanced   50%      1%        5 %
        }
        for (String name : greedyWallets) {
            simulations.put(name, new WalletSimulation(name, allWalletNames, GREEDY_PROBABILITY,
                    GREEDY_MIN, GREEDY_MAX, GREEDY_ADAPTION));
        }
        for (String name : balancedWallets) {
            simulations.put(name, new WalletSimulation(name, allWalletNames, BALANCED_PROBABILITY,
                    BALANCED_MIN, BALANCED_MAX, BALANCED_ADAPTION));
        }
        try {
            this.nextTransaction();
        } catch (InterruptedException ex) {
            Exceptions.printStackTrace(ex);
        }
        simulationFinished.future().andThen(new OnComplete<Void>() {
            @Override
            public void onComplete(Throwable failure, Void success) throws Throwable {
                final boolean noDelay = false;
                GGBWalletConfiguration.this.makeSnapshot(noDelay).future().onSuccess(
                        new OnSuccess<Snapshot>() {
                            @Override
                            public void onSuccess(Snapshot snap) throws Throwable {
                                JSONObject json = snap.toJson();
                                TimeZone tz = TimeZone.getTimeZone("UTC");
                                DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
                                df.setTimeZone(tz);
                                String nowAsISO = df.format(new Date());

                                Path logFilename = Paths.get(GGBWalletConfiguration.logDirectory, nowAsISO + ".json");

                                try(PrintWriter f = new PrintWriter(logFilename.toString())) {
                                    f.println(json.toString());
                                }
                                System.out.println("SNAPSHOT saved under: " + logFilename.toString());
                            }
                        }
                , context().dispatcher());
            }
        }, context().dispatcher());
    }
    private void nextTransaction() throws InterruptedException {
        for(;currentStep < n_steps; currentStep++) {

            for (WalletSimulation currentWallet : simulations.values()) {
                if(currentWallet.willSend()){
                    String receiverName = currentWallet.sampleReceiver();
                    String senderName = currentWallet.getName();

                    ActorRef senderWallet = walletMap.get(senderName);
                    ActorRef receiverWallet = walletMap.get(receiverName);

                    int value = currentWallet.sampleValue(getMoney(senderWallet));

                    sendMoney(value, receiverWallet, senderWallet);

                    WalletSimulation receiverSimulation = simulations.get(receiverName);
                    receiverSimulation.updateProbabily(currentWallet.getName(), value);

                    this.makeSnapshot().future().onSuccess(
                        new OnSuccess<Snapshot>() {
                            @Override
                            public void onSuccess(Snapshot snap) throws Throwable {
                                System.out.println("SNAPSHOT: " + snap);
                            }
                        }
                    , context().dispatcher());
                    return;
                }
            }
        }
        if (simulationFinished != null) {
            simulationFinished.success(null);
            simulationFinished = null;
        }
    }


    private void sendMoney(int transferAmount,ActorRef receiverWallet, ActorRef senderWallet) {
        System.out.println(receiverWallet.path().name());
        senderWallet.tell(new ActionWalletSendMoney(receiverWallet.path().name(), transferAmount, self()), self());
    }
    private int getMoney(ActorRef sender) {
        Future<Object> future = Patterns.ask(sender, new ActionGetAmount(), timeout);
        try {
            ActionGetAmountAnswer answer = (ActionGetAmountAnswer) Await.result(future, timeout.duration());
            return answer.amount;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public void onReceive(Object message) {
        if (message instanceof ActionNotifyObserver) {
            ActionNotifyObserver notification = (ActionNotifyObserver) message;

            String status = "successful";
            if (!notification.granted) {
                status = "failed";
            }

            System.out.println("Observed a " + status + " transaction of " + notification.amount + " FUCs from " +
                    notification.source.path().name() + " to " + notification.target.path().name());

            if (currentStep < n_steps) {
                try {
                    nextTransaction();
                } catch (InterruptedException ex) {
                    Exceptions.printStackTrace(ex);
                }
            } else {
                if (simulationFinished != null) {
                    simulationFinished.success(null);
                    simulationFinished = null;
                }
            }
        }
        if (message instanceof SnapAction) {
            ((SnapAction) message).doAction(this);
        }
    }
}
