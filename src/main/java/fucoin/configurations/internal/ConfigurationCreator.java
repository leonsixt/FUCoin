package fucoin.configurations.internal;

import akka.japi.Creator;
import fucoin.configurations.AbstractConfiguration;

/**
 * This is used by Akka to spawn the Configuration Actor
 */
public class ConfigurationCreator implements Creator<AbstractConfiguration> {

    private Class configurationClass;

    public ConfigurationCreator(Class configurationClass) {

        this.configurationClass = configurationClass;
    }

    @Override
    public AbstractConfiguration create() throws Exception {
        return (AbstractConfiguration) this.configurationClass.newInstance();
    }
}
