package fucoin.configurations.internal;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * This Annotation can be used by Configurations to display
 * a more friendly title than the class name in the startup dialog
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface ConfigurationName {
    String value();
}
