package fucoin.configurations.internal;

import fucoin.configurations.AbstractConfiguration;

/**
 * A ConfigurationSelection is a wrapper around the configuration class objects
 * to display a custom text in the configuration selection dropdown.
 */
public class ConfigurationSelection {
    private Class<AbstractConfiguration> configurationClass;

    public ConfigurationSelection(Class<AbstractConfiguration> configurationClass) {
        this.configurationClass = configurationClass;
    }

    public Class<AbstractConfiguration> getConfigurationClass() {
        return configurationClass;
    }

    @Override
    public String toString() {
        if (configurationClass.isAnnotationPresent(ConfigurationName.class)) {
            return configurationClass.getAnnotation(ConfigurationName.class).value();
        }
        return configurationClass.getSimpleName();
    }
}
