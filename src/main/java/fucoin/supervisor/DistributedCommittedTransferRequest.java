package fucoin.supervisor;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.SnapshotToken;
import fucoin.actions.transaction.Transaction;
import fucoin.wallet.AbstractWallet;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class DistributedCommittedTransferRequest extends Transaction {
    private final static Random random = new Random(System.currentTimeMillis() + System.nanoTime());
    private ActorRef source;
    private ActorRef target;
    private ActorRef observer;

    private int amount;

    private long timeout;
    private long id;
    private final SnapshotToken snapshotToken;

    private List<ActorRef> answers = new LinkedList<>();

    public DistributedCommittedTransferRequest(ActorRef source, ActorRef target, int amount,
                                               long timeout, SnapshotToken snapshotToken, ActorRef observer) {
        this.source = source;
        this.target = target;
        this.amount = amount;

        this.timeout = timeout;
        this.id = random.nextLong();
        this.snapshotToken = snapshotToken;

        this.observer = observer;
    }

    @Override
    protected void onAction(ActorRef sender, ActorRef self,
                            UntypedActorContext context, AbstractWallet wallet) {

    }

    public ActorRef getSource() {
        return source;
    }

    public ActorRef getTarget() {
        return target;
    }

    public long getTimeout() {
        return timeout;
    }

    public int addPositiveAnswer(ActorRef sender) {
        answers.add(sender);
        return answers.size();
    }

    public List<ActorRef> getAnswers() {
        return answers;
    }

    public long getId() {
        return id;
    }

    public int getAmount() {
        return amount;
    }

    public SnapshotToken getSnapshotToken() {
        return snapshotToken;
    }

    public ActorRef getObserver(){
        return observer;
    }

}
