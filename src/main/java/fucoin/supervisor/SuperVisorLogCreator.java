package fucoin.supervisor;

import akka.japi.Creator;
import fucoin.gui.SuperVisorGuiControl;
import fucoin.gui.SuperVisorGuiControlImpl;

/**
 * Create SuperVisor with a AWT Window.
 * The window displays the information from the supervisor.
 */
public class SuperVisorLogCreator implements Creator<SuperVisorImpl> {
    private final String filename;

    public SuperVisorLogCreator(String filename) {
        this.filename = filename;
    }

    @Override
    public SuperVisorImpl create() throws Exception {
        SuperVisorImpl sv = new SuperVisorLogImpl(this.filename);
        SuperVisorGuiControl superVisorGuiControl = new SuperVisorGuiControlImpl(sv);
        sv.setGuiControl(superVisorGuiControl);
        return sv;
    }

}
