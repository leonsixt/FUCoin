package fucoin.supervisor;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.Vector;

public class AmountTableModel extends DefaultTableModel {

    public AmountTableModel() {
        super(new Object[]{"Address", "Name", "Amount"}, 0);
        this.addRow(new Object[]{"", "Global", 0} );
    }

    public void clear() {
        while (getRowCount() > 0) {
            removeRow(0);
        }
    }

    private void calculateGlobalState(){
    	int globalCount=0;
    	Vector rows = this.getDataVector();
        for (int i = 1; i < rows.size(); i++) {
            if (rows.get(i) instanceof Vector) {
                Vector<Object> row = (Vector<Object>) rows.get(i);
                globalCount+=(int)row.get(2);
            }
        }
        setValueAt(globalCount, 0, 2);
    }

    public void updateTable(String address, String name, int amount) {

        Vector rows = this.getDataVector();
        for (int i = 1; i < rows.size(); i++) {
            if (rows.get(i) instanceof Vector) {
                Vector<Object> row = (Vector<Object>) rows.get(i);
                if (row.get(0).equals(address)) {
                    setValueAt(amount, i, 2);
                    calculateGlobalState();
                    return;
                }
            }
        }

        this.addRow(new Object[]{address, name, amount});
        calculateGlobalState();
    }
}