package fucoin.supervisor;

import akka.actor.Props;
import fucoin.actions.transaction.ActionGetAmountAnswer;
import fucoin.gui.SuperVisorGuiControl;
import fucoin.gui.SuperVisorGuiControlImpl;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class LogEntry {
    public long timestamp;
    public Object action;
    public LogEntry(Object action) {
        this.timestamp = System.currentTimeMillis();
        this.action = action;
    }
    public JSONObject to_json() {
        JSONObject obj = new JSONObject()
                .put("timestamp", this.timestamp);
        if (this.action instanceof ActionGetAmountAnswer) {
            ActionGetAmountAnswer answer = (ActionGetAmountAnswer) this.action;
            obj.put("address", answer.address);
            obj.put("amount", answer.amount);
            obj.put("name", answer.name);
        }
        return obj;
    }
}

public class SuperVisorLogImpl extends SuperVisorImpl {
    private final String filename;
    private List<LogEntry> log;

    public SuperVisorLogImpl(String filename) {
        this.filename = filename;
        this.log = new ArrayList<>();
    }

    @Override
    public void onReceive(Object msg) {
        super.onReceive(msg);
        // System.out.println("####################################");
        // System.out.println(msg);
        if (msg instanceof ActionGetAmountAnswer) {
            ActionGetAmountAnswer answer = (ActionGetAmountAnswer) msg;
            log.add(new LogEntry(answer));
        }
        try {
            this.save(this.filename);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    public static Props props(String filename) {
        return Props.create(new SuperVisorLogCreator(filename));
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        this.save(this.filename);
    }

    public void save(String filename) throws FileNotFoundException {
        JSONArray arr = new JSONArray();
        for (LogEntry entry : this.log) {
            arr.put(entry.to_json());
        }
        try(PrintWriter f = new PrintWriter(filename)) {
            f.println(arr.toString());
        }
    }
}
