package fucoin;

import akka.actor.ActorSystem;
import akka.actor.Props;
import fucoin.configurations.AbstractConfiguration;
import fucoin.configurations.internal.ConfigurationSelection;

import java.util.List;

public class MainSimulation extends Main {
    public static void main(String[] args) throws InterruptedException, IllegalAccessException, InstantiationException {
        List<ConfigurationSelection> configurations = getAbstractConfigurations();

        ConfigurationSelection[] configs = new ConfigurationSelection[configurations.size()];
        configurations.toArray(configs);
        ConfigurationSelection selectedConfig = null;
        for (ConfigurationSelection config : configurations) {
            if (config.toString().equals("GGBWallet")) {
                selectedConfig = config;
                break;
            }
        }
        if (selectedConfig != null) {
            // The Configuration will be an actor in the system, so we tell akka to create the actor
            Props theProps = AbstractConfiguration.props(selectedConfig.getConfigurationClass());

            cSystem.actorOf(theProps, "Configuration");
        } else {
            System.err.println("Did not find configuration GGBWallet");
            cSystem.terminate();
        }
    }
}
