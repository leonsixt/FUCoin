package fucoin;

import akka.dispatch.Futures;
import fucoin.actions.snap.SnapShotSaveState;
import org.json.JSONObject;
import scala.concurrent.Promise;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class Snapshot {
    public Map<String, Integer> states = new ConcurrentHashMap<String, Integer>();
    public List<SnapshotTransaction> transactions = Collections.synchronizedList(new ArrayList<>());
    private Promise<Snapshot> complete = Futures.promise();


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Snapshot {\n");
        sb.append("  States {\n");
        for (String name : states.keySet()) {
            sb.append("    " + name + ": " + states.get(name) + "\n");
        }
        sb.append("  }\n");
        sb.append("  Transactions {\n");
        for (int i = 0; i < transactions.size(); i++) {
            SnapshotTransaction ts = transactions.get(i);
            sb.append("    " +  ts.amount + " FU's from " + ts.sender + " to " + ts.receiver + "\n");
        }
        sb.append("  }\n");
        sb.append("}\n");

        return sb.toString();
    }

    public JSONObject toJson() {
        JSONObject obj = new JSONObject();
        for (String key : states.keySet()) {
            obj.append(key, states.get(key));
        }
        return obj;
    }

    public void addState(String name, int amount) {
        states.put(name, amount);
    }

    public void addState(SnapShotSaveState state) {
        states.put(state.name, state.amount);
    }

    public void addTransaction(SnapshotTransaction snapshotTransaction) {
        transactions.add(snapshotTransaction);
    }

    public void completed() {
        this.complete.success(this);
    }
    public Promise<Snapshot> promise() {
        return this.complete;
    }
}
