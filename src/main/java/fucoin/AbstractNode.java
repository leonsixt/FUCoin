package fucoin;

import akka.actor.ActorRef;
import akka.actor.Address;
import akka.actor.UntypedActor;

import fucoin.actions.snap.SnapShotBegin;
import fucoin.actions.transaction.ActionGetAmount;
import fucoin.supervisor.SuperVisorImpl;
import fucoin.wallet.AbstractWallet;
import scala.concurrent.Promise;

import java.io.Serializable;
import java.util.HashMap;
import org.openide.util.Exceptions;

public abstract class AbstractNode extends UntypedActor implements Serializable {
    protected SnapshotToken currentSnapshotToken;


    protected Snapshot snapshot;

    /**
     * Returns the akka-style address as String,
     * which could be converted to an ActorRef object later
     *
     * @return
     */
    public String getAddress() {
        return getAddress(getSelf());
    }

    public String getAddress(ActorRef self) {
        Address remoteAddr = getContext().system().provider().getDefaultAddress();
        return self.path().toStringWithAddress(remoteAddr);
    }

    /**
     * The which receives Action objects
     *
     * @param message
     */
    public abstract void onReceive(Object message);

    /**
     * Holds references to neighbors that were in contact with this wallet during runtime;
     * The key corresponds to the WalletImpl's name
     */
    private transient HashMap<String, ActorRef> knownNeighbors = new HashMap<String, ActorRef>();

    /**
     * Holds references to neighbors
     * this wallet synchronizes itself to (the WalletImpl object);
     * The key corresponds to the WalletImpl's name
     */
    public transient HashMap<String, ActorRef> localNeighbors = new HashMap<String, ActorRef>();

    /**
     * Holds all Wallets from network participants
     * which synchronize their state (WalletImpl object) with us;
     * The key corresponds to the WalletImpl's name
     */
    public transient HashMap<String, AbstractWallet> backedUpNeighbors =
            new HashMap<String, AbstractWallet>();

    public transient HashMap<ActorRef, Integer> amounts = new HashMap<ActorRef, Integer>();

    public boolean addKnownNeighbor(String key, ActorRef value) {
        if (!knownNeighbors.containsKey(key)) {
            knownNeighbors.put(key, value);
            value.tell(new ActionGetAmount(), getSelf());
            return true;
        }
        return false;
    }

    public SnapshotToken getCurrentSnapshotToken() {
        return currentSnapshotToken;
    }

    public void setCurrentSnapshotToken(SnapshotToken currentSnapshotToken) {
        this.currentSnapshotToken = currentSnapshotToken;
    }

    public Snapshot getSnapshot() {
        return snapshot;
    }

    public HashMap<String, ActorRef> getKnownNeighbors() {
        return knownNeighbors;
    }


    public Promise<Snapshot> makeSnapshot() throws InterruptedException {
        return makeSnapshot(true);
    }

    public Promise<Snapshot> makeSnapshot(boolean useDelay) throws InterruptedException{
        String prefix = "";
        if (this instanceof SuperVisorImpl) {
            prefix = "supervisor";
        } else if (this instanceof AbstractWallet) {
            AbstractWallet wallet = (AbstractWallet) this;
            prefix = wallet.getName();
        }
        String snapName = prefix + "_" + String.valueOf(System.currentTimeMillis());
        this.currentSnapshotToken = new SnapshotToken(snapName, self());
        this.snapshot = new Snapshot();
        if (this instanceof AbstractWallet) {
            AbstractWallet wallet = (AbstractWallet) this;
            this.snapshot.addState(wallet.getName(), wallet.getAmount());
        }
        //Broadcast
        int i = 0;
        for (ActorRef act : getKnownNeighbors().values()) {
           if(i % 3 == 0){
               if (useDelay) {
                   try {
                       //Add a little useDelay now and then
                       Thread.sleep(500);
                   } catch (InterruptedException ex) {
                       Exceptions.printStackTrace(ex);
                   }
               }
           }
           act.tell(new SnapShotBegin(this.currentSnapshotToken), self());
        }
        return this.snapshot.promise();
    }
}