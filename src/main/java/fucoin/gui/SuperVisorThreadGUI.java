package fucoin.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 *
 */
public class SuperVisorThreadGUI {
    private JFrame frame;

    private FilteredLogModel log = new FilteredLogModel();
    private JList<LogMessage> txtLog = new JList<>(log);
    private JScrollPane logPane = new JScrollPane(txtLog);
    private JCheckBox showDebug;
    private JCheckBox activateLogging;
    private SuperVisorGuiControlImpl superVisorGuiControl;

    public SuperVisorThreadGUI(SuperVisorGuiControlImpl superVisorGuiControl) {

        this.superVisorGuiControl = superVisorGuiControl;
    }

    public void init() {
        new Thread(() -> {
            //Show AWT window for runtime information
            frame = new JFrame("Server");
            JPanel contentPanel = new JPanel();
            contentPanel.setLayout(new GridLayout(2, 1));

            JTable amountListView = new JTable(superVisorGuiControl.getAmountTableModel());
            superVisorGuiControl.getAmountTableModel().addTableModelListener(e -> SwingUtilities.invokeLater(() -> frame.setTitle("Server (" + superVisorGuiControl.getAmountTableModel().getRowCount() + " Wallets)")));
            contentPanel.add(new JScrollPane(amountListView));

            JPanel logPanel = new JPanel(new BorderLayout());

            txtLog.setCellRenderer(new LogCellRenderer());

            showDebug = new JCheckBox("Show debug messages in transaction log");
            showDebug.setSelected(true);
            showDebug.addItemListener(e -> {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    log.clearFilter();
                } else {
                    log.setTransactionFilter();
                }
            });

            activateLogging = new JCheckBox("Activate logging");
            activateLogging.setSelected(superVisorGuiControl.isLogActive());
            activateLogging.addItemListener(e -> {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    superVisorGuiControl.activateLogging();
                    log.emptyLog();
                } else {
                    superVisorGuiControl.disableLogging();
                }
            });

            JPanel configPanel = new JPanel();

            configPanel.add(activateLogging);
            configPanel.add(showDebug);

            //logPanel.add(activateLogging, BorderLayout.NORTH);
            logPanel.add(configPanel, BorderLayout.NORTH);
            logPanel.add(logPane, BorderLayout.CENTER);
            contentPanel.add(logPanel);

            frame.add(contentPanel, BorderLayout.CENTER);

            //Exit Button and shutdown supervisor
            JButton exitBtn = new JButton("Stop Supervisor");
            exitBtn.addActionListener(e -> {
                superVisorGuiControl.guiTerminated();
                frame.setVisible(false);
                frame.dispose();
            });

            if (!superVisorGuiControl.isLogActive()) {
                this.log(new LogMessage("Logging is currently disabled."));
            }

            frame.add(exitBtn, BorderLayout.PAGE_END);
            frame.setSize(800, 600);
            frame.setVisible(true);

            frame.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    super.windowClosing(e);
                    superVisorGuiControl.guiTerminated();
                }
            });
        }).start();
    }

    public void dispose() {
        frame.dispose();
    }

    public void log(LogMessage logMessage) {
        SwingUtilities.invokeLater(() -> {
            log.addElement(logMessage);

            // auto scroll to the bottom
            txtLog.ensureIndexIsVisible(log.getSize() - 1);
        });
    }
}
