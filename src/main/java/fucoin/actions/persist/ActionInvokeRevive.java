package fucoin.actions.persist;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.actions.join.ActionJoin;
import fucoin.wallet.AbstractWallet;

public class ActionInvokeRevive extends Persist {

    @Override
    protected void onAction(ActorRef sender, ActorRef self,
                            UntypedActorContext context, AbstractWallet wallet) {
        wallet.setActive(true);
        wallet.getPreKnownNeighbour().tell(new ActionJoin(), self);
    }

}
