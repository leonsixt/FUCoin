package fucoin.actions.persist;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.wallet.AbstractWallet;
import fucoin.wallet.WalletImpl;

//Used to push the state of my/a wallet to another participant
public class ActionStoreOrUpdate extends Persist {
    public final AbstractWallet w;

    public ActionStoreOrUpdate(AbstractWallet w) {
        this.w = w;
    }

    @Override
    protected void onAction(ActorRef sender, ActorRef self,
                            UntypedActorContext context, AbstractWallet wallet) {
        wallet.backedUpNeighbors.put(w.getName(), w);
    }
}
