package fucoin.actions.persist;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.wallet.AbstractWallet;

// Used to search a WalletImpl by name, i.e. the own wallet if we just
// joined the network; If a receiving participant holds the stored WalletImpl,
// he returns it, otherwise, he might use gossiping methods to go on 
// with the search;
// Note: You should also forward the sender (the participant who actually
// searches for this WalletImpl, so that it can be returnd the direct way)
public class ActionSearchMyWallet extends Persist {
    public final String name;

    public ActionSearchMyWallet(String name) {
        this.name = name;
    }

    @Override
    protected void onAction(ActorRef sender, ActorRef self,
                            UntypedActorContext context, AbstractWallet wallet) {
        wallet.addKnownNeighbor(name, sender);
        AbstractWallet storedWallet = wallet.backedUpNeighbors.get(name);
        if (storedWallet != null) {
            sender.tell(new ActionSearchMyWalletAnswer(storedWallet), self);
        }
    }
}
