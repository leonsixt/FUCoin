package fucoin.actions.snap;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.AbstractNode;
import fucoin.actions.ClientAction;
import fucoin.wallet.AbstractWallet;

public class SnapShotEnd extends SnapAction {
    @Override
    protected void onAction(ActorRef sender, ActorRef self, UntypedActorContext context, AbstractNode abstractNode) {
        abstractNode.setCurrentSnapshotToken(null);
    }
}
