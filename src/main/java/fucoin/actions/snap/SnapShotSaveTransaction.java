package fucoin.actions.snap;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.AbstractNode;
import fucoin.SnapshotTransaction;

public class SnapShotSaveTransaction extends SnapAction {
    public SnapshotTransaction transaction;

    public SnapShotSaveTransaction(SnapshotTransaction transaction) {
        this.transaction = transaction;
    }

    @Override
    protected void onAction(ActorRef sender, ActorRef self,
                            UntypedActorContext context, AbstractNode abstractNode) {
        abstractNode.getSnapshot().addTransaction(transaction);
    }
}
