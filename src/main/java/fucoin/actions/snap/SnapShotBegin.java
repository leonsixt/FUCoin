package fucoin.actions.snap;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.AbstractNode;
import fucoin.SnapshotToken;
import fucoin.actions.Action;
import fucoin.actions.ClientAction;
import fucoin.wallet.AbstractWallet;

public class SnapShotBegin extends SnapAction {
    private SnapshotToken snapshotToken;

    public SnapShotBegin(SnapshotToken snapshotToken) {
        this.snapshotToken = snapshotToken;
    }

    @Override
    protected void onAction(ActorRef sender, ActorRef self, UntypedActorContext context, AbstractNode abstractNode) {
        abstractNode.setCurrentSnapshotToken(this.snapshotToken);
        if (abstractNode instanceof AbstractWallet) {
            AbstractWallet wallet = (AbstractWallet) abstractNode;
            sender.tell(new SnapShotSaveState(wallet.getName(), wallet.getAmount()), self);
        }
    }
}
