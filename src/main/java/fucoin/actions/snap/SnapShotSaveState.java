package fucoin.actions.snap;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.AbstractNode;
import fucoin.Snapshot;
import fucoin.actions.Action;

import java.util.Set;

public class SnapShotSaveState extends SnapAction {
    public String name;
    public int amount;

    public SnapShotSaveState(String name, int amount) {
        this.name = name;
        this.amount = amount;
    }
    @Override
    protected void onAction(ActorRef sender, ActorRef self, UntypedActorContext context,
                            AbstractNode abstractNode) {
        Snapshot snap = abstractNode.getSnapshot();
        snap.addState(this);
        Set<String> neighbors = abstractNode.getKnownNeighbors().keySet();
        if (snap.states.keySet().containsAll(neighbors)) {
            for (ActorRef node : abstractNode.getKnownNeighbors().values()) {
                node.tell(new SnapShotEnd(), self);
            }
            try {
                if (! snap.promise().isCompleted()) {
                    snap.completed();
                }
            } catch (IllegalStateException e) {
                throw e;
            }
        }
    }
}
