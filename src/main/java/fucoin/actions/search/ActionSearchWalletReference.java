package fucoin.actions.search;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.wallet.AbstractWallet;

import java.util.ArrayList;
import java.util.List;

/**
 * Used to return a WalletImpl reference (akka-style string which can
 * be transformed to an ActorRef)
 */
public class ActionSearchWalletReference extends Search {

    public final String name;
    public final List<ActorRef> ttl = new ArrayList<>();

    public ActionSearchWalletReference(String name) {
        this.name = name;
    }

    @Override
    protected void onAction(ActorRef sender, ActorRef self,
                            UntypedActorContext context, AbstractWallet wallet) {
        wallet.addLogMsg(wallet.getKnownNeighbors() + "contains " + name + "?");
        ttl.add(self);
        ActionSearchWalletReferenceAnswer answer = null;
        if (this.name.equals(wallet.getName())) {
            answer = new ActionSearchWalletReferenceAnswer(name, wallet.getAddress(), ttl);
        } else if (wallet.backedUpNeighbors.containsKey(name)) {
            answer = new ActionSearchWalletReferenceAnswer(name, wallet.backedUpNeighbors.get(name).getAddress(), ttl);
        } else if (wallet.getKnownNeighbors().containsKey(name)) {
            answer = new ActionSearchWalletReferenceAnswer(name, wallet.getAddress(wallet.getKnownNeighbors().get(name)), ttl);
        } else if (ttl.size() < 5) {
            for (ActorRef neighbor : wallet.getKnownNeighbors().values()) {
                if (!ttl.contains(neighbor)) {
                    neighbor.tell(this, self);
                }
            }
        }
        //User unknown by this WalletImpl
        if (answer != null && ttl.size() > 0) {
            ttl.get(ttl.size() - 1).tell(answer, self);
        }
    }

}
