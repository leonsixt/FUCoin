package fucoin.actions.search;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.wallet.AbstractWallet;

import java.util.List;

public class ActionSearchWalletReferenceAnswer extends Search {

    public final String address;
    public final String name;
    public final List<ActorRef> pathToSearchedWallet;

    public ActionSearchWalletReferenceAnswer(String name, String address, List<ActorRef> pathToSearchedWallet) {
        this.address = address;
        this.name = name;
        this.pathToSearchedWallet = pathToSearchedWallet;
    }

    @Override
    protected void onAction(ActorRef sender, ActorRef self,
                            UntypedActorContext context, AbstractWallet wallet) {
        ActorRef target = context.actorSelection(address).anchor();
        wallet.addKnownNeighbor(name, target);
        int pos = pathToSearchedWallet.indexOf(self);
        if (pos > 0) {
            pathToSearchedWallet.get(pos - 1).tell(this, self);
        }
    }
}
