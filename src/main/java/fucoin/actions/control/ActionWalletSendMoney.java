package fucoin.actions.control;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.SnapshotToken;
import fucoin.actions.ClientAction;
import fucoin.wallet.AbstractWallet;

/**
 *
 */
public class ActionWalletSendMoney extends ClientAction {

    protected String address;
    protected int amount;

    protected ActorRef observer;

    public ActionWalletSendMoney(String address, int amount, ActorRef observer) {
        this.address = address;
        this.amount = amount;
        this.observer = observer;
    }

    public ActionWalletSendMoney(String address, int amount){
        this(address, amount, null);
    }

    @Override
    protected void onAction(ActorRef sender, ActorRef self, UntypedActorContext context, AbstractWallet abstractNode) {
        abstractNode.send(address, amount, observer);
    }
}
