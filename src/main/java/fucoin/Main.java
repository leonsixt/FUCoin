package fucoin;

import akka.actor.ActorSystem;
import akka.actor.Props;
import com.google.common.collect.Sets;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import fucoin.configurations.AbstractConfiguration;
import fucoin.configurations.internal.ConfigurationSelection;
import fucoin.setup.NetworkInterfaceReader;
import org.reflections.ReflectionUtils;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeElementsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import javax.swing.*;
import java.io.File;
import java.lang.reflect.Modifier;
import java.util.*;


public class Main {

    protected static ActorSystem cSystem;

    static {
        String hostname = NetworkInterfaceReader.readDefaultHostname();

        //Load configuration from current directory or from resources directory of jar
        File file = new File("application.conf");
        Config config = ConfigFactory.parseFile(file);
        if (!file.exists()) {
            System.out.println("Load default application.conf");
            config = ConfigFactory.parseResources("application.conf");
        } else {
            System.out.println("Load local application.conf");
        }

        //Init System Actor System
        cSystem = ActorSystem.create("Core", ConfigFactory.parseString("akka.remote.netty.tcp.hostname=" + hostname).withFallback(config));
    }

    public static void main(String[] args) throws InterruptedException, IllegalAccessException, InstantiationException {
        List<ConfigurationSelection> configurations = getAbstractConfigurations();

        ConfigurationSelection[] configs = new ConfigurationSelection[configurations.size()];
        configurations.toArray(configs);

        // Display the selection dialog to select a configuration
        ConfigurationSelection selectedConfig = (ConfigurationSelection) JOptionPane.showInputDialog(
                null,
                "Select a configuration to run",
                "Configuration Selection",
                JOptionPane.QUESTION_MESSAGE,
                null,
                configs,
                configurations.get(0)
        );

        if (selectedConfig != null) {
            // The Configuration will be an actor in the system, so we tell akka to create the actor
            Props theProps = AbstractConfiguration.props(selectedConfig.getConfigurationClass());

            cSystem.actorOf(theProps, "Configuration");
        } else {
            cSystem.terminate();
        }
    }

    /**
     * This method crawls the fucoin.configurations package for classes extending the AbstractConfiguration.
     *
     * @return The list contains all non abstract extensions of AbstractConfiguration in the namespace, wrapped in ConfigurationSelection objects
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    protected static List<ConfigurationSelection> getAbstractConfigurations() throws InstantiationException, IllegalAccessException {
        List<ClassLoader> classLoadersList = new LinkedList<>();
        // Lots of reflection magic happening here!
        classLoadersList.add(ClasspathHelper.contextClassLoader());
        classLoadersList.add(ClasspathHelper.staticClassLoader());

        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setScanners(new SubTypesScanner(false), new ResourcesScanner(), new TypeElementsScanner())
                .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
                .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix("fucoin.configurations"))));

        Set<String> typeSet = reflections.getStore().get("TypeElementsScanner").keySet();
        HashSet<Class<?>> allClasses = Sets.newHashSet(ReflectionUtils.forNames(typeSet, reflections
                .getConfiguration().getClassLoaders()));

        List<ConfigurationSelection> configurations = new ArrayList<>();

        // Filter the found classes for non abstract classes, that inherit from AbstractConfiguration
        allClasses.stream().filter(oneClass -> !Modifier.isAbstract(oneClass.getModifiers()) && AbstractConfiguration.class.isAssignableFrom(oneClass)).forEach(oneClass -> {
            ConfigurationSelection cfg = new ConfigurationSelection((Class<AbstractConfiguration>) oneClass);
            configurations.add(cfg);
        });

        return configurations;
    }
}
